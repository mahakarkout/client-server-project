﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

public class Client
{
    public void SendMessage(string serverIp, int port, string message)
    {
        TcpClient client = new TcpClient(serverIp, port);
        NetworkStream stream = client.GetStream();

        byte[] data = Encoding.ASCII.GetBytes(message);
        stream.Write(data, 0, data.Length);

        client.Close();
    }
}
