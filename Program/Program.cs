﻿using System;



class Program
{
    static void Main(string[] args)
    {
        Server server = new Server(8888);
        Client client = new Client();

        // Start the server in a separate thread
        System.Threading.Thread serverThread = new System.Threading.Thread(server.Start);
        serverThread.Start();

        // Send a message from the client to the server
        client.SendMessage("127.0.0.1", 8888, "Hello from the client!");

        // Wait for the user to press Enter before stopping the server
        Console.WriteLine("Press Enter to stop the server...");
        Console.ReadLine();

        // Stop the server
        server.Stop();
    }
}
